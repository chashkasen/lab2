#include <iostream>
#include <chrono>
#include <timer.hpp>
#pragma comment(linker, "/STACK:40000000")

#define N 1000000


void QuickSort(int a, int b, int mas[N])
{

	if (a >= b)
		return;
	int m = ((rand() * rand()) % (b - a + 1)) + a;
	int k = mas[m];
	int l = a - 1;
	int r = b + 1;
	while (1)
	{
		do l = l + 1; while (mas[l] < k);
		do r = r - 1; while (mas[r] > k);
		if (l >= r)
			break;
		std::swap(mas[l], mas[r]);
	}
		r = l;
		l--;
	
		QuickSort(a, l, mas);
		QuickSort(r, b, mas);

}

void BubbleSort(int mas[N], int n)
{
	for (int i = 1; i < n; i++)
	{
		if (mas[i] >= mas[i - 1])
			continue;
		int j = i - 1;
		while (j >= 0 && mas[j] > mas[j + 1])
		{
			std::swap(mas[j], mas[j + 1]);
			j--;
		}
	}
}



int main()
{
	srand(time(0));
	int mas[N], n;


	std::cin >> n;

	for (int i = 0; i < n; i++)
		mas[i] = rand();

	std::cout << "Quick, "<< n << std::endl;

	Timer t1;
	QuickSort(0, n-1, mas);
	std::cout << "Time elapsed: " << t1.elapsed() << '\n';


	for (int i = 0; i < n; i++)
		mas[i] = rand();

	std::cout << "Bubble, " << n << std::endl;

	Timer t2;
	BubbleSort(mas, n);
	std::cout << "Time elapsed: " << t2.elapsed() << '\n';

	return 0;
}